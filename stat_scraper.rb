require 'rubygems'
require 'hpricot'
require 'uri'
require 'open-uri'

class StatScraper
  attr_reader :league_url, :team_url, :team_name, :league, :schedule_stats, :passing_stats, :skilled_stats

  def initialize(stats_url, team_name, league=nil)
    @stats_url = stats_url
    @team_name = team_name
    @league = league

    @base_url = get_base_url(@stats_url)

    @league_url = nil
    @team_url = nil
    @schedule_stats = nil
    @passing_stats = nil
    @skilled_stats = nil

    parse()
  end

  def get_base_url(stats_url)
    uri = URI(stats_url)
    "#{uri.scheme}://#{uri.host}"
  end

  def load_hpricot_page(url)
    page = Hpricot(open(url))
  end

  #matchups
  def extract_schedule_stats(team_page)
    return true
  end

  #passing
  def extract_passing_stats(hpr_team_page)
    stats_table = hpr_team_page.search("table tr[text()='Passing Totals']").first.parent

    #parse stat types from table
    stats = extract_stats_from_table(stats_table)
  end

  #receiving/defense
  def extract_skilled_stats(hpr_team_page)
    stats_table = hpr_team_page.search("table tr[text()='Receiving/Defensive Totals']").first.parent
    
    stats = extract_stats_from_table(stats_table)    
  end

  def extract_stats_from_table(stats_table)
    
    #parse stat types from table
    stats_table_rows = stats_table.children_of_type('tr')

    stat_type_row = stats_table_rows[1]
    stat_types_hash = extract_stat_types_from_row(stat_type_row)

    stats = Hash.new

    #go thru stat rows till there aint no more
    current_stat_row = stat_type_row.next_sibling
    while current_stat_row != nil
      #get stat tds
      stat_tds = current_stat_row.search('td')

      #set player name as key
      player_name = stat_tds[1].inner_html
      stats[player_name] = stat_types_hash.clone

      #loop thru stat hash and populate
      stat_index = 0
      stats[player_name].each do |stat_type, val|
        stats[player_name][stat_type] = stat_tds[stat_index].inner_html
        stat_index += 1
      end

      current_stat_row = current_stat_row.next_sibling
    end

    stats
  end

  def extract_stat_types_from_row(stat_type_row)
    stat_types = Hash.new

    stat_type_row.search('td').each do |stat_type_td|
      stat_types[stat_type_td.search('b').inner_html] = 0
    end

    stat_types
  end


  #example: <a href="league_week_results.php?idLeague=63&idSeason=26&Week=8">Pomona</a>
  def extract_league_url(url, league)
    search_str = "a[@href*='league_week_results.php'][text()='#{league}']"

    main_page = load_hpricot_page(url)

    league_urls = main_page.search(search_str)
    league_url = league_urls.first.attributes['href']

    "#{@base_url}/#{league_url}"
  end

  #example: <a href="team_summary.php?idTeam=5614&idSeason=26&GameWeek=8">Stop Lying</a>
  def extract_team_url(league_url, team_name)
    search_str = "a[@href*='team_summary.php'][text()='#{team_name}']"

    league_page = load_hpricot_page(league_url)

    team_urls = league_page.search(search_str)
    team_url = team_urls.first.attributes['href']

    "#{@base_url}/#{team_url}"
  end

  def parse()
    @league_url = extract_league_url(@stats_url,@league)
    @team_url = extract_team_url(@league_url, @team_name)

    team_page = load_hpricot_page(@team_url)
    @schedule_stats = extract_schedule_stats(team_page)
    @passing_stats = extract_passing_stats(team_page)
    @skilled_stats = extract_skilled_stats(team_page)
  end
end
